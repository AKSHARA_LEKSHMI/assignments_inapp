# TIC TAC TOE GAME
import random
board = {'1':" ",'2':" ",'3':" ",'4':" ",'5':" ",'6':" ",'7':" ",'8':" ",'9':" "}
def printboard(board):
    print(board['1']+'|'+board['2']+'|'+board['3'])
    print('-+-+-')
    print(board['4'] + '|' + board['5'] + '|' + board['6'])
    print('-+-+-')
    print(board['7'] + '|' + board['8'] + '|' + board['9'])
def player_moves(*args) :
    player_input = input('enter the position:')
    while board[player_input] != ' ' :
        player_input = input('The position is occupied, enter the position again:')
    board[player_input] = 'X'
def computer_moves(*args) :
    computer_move = random.choice(['1','2','3','4','5','6','7','8','9'])
    while board[computer_move] != ' ' :
        computer_move = random.choice(['1','2','3','4','5','6','7','8','9'])
    board[computer_move] = 'O'
    print('computer move {}'.format(computer_move))
def game(*args):
    count = 0
    for i in range(0,5) :
        player_moves()
        printboard(board)
        count += 1
        if count>4 :
            result_board = board
            break
        else :
            computer_moves()
            printboard(board)
def check_result(*args):
    if board['1']==board['2']==board['3']=="X" or board['4']==board['5']==board['6']=="X" or board['7']==board['8']==board['9']=="X" or board['1']==board['4']==board['7']=="X" or board['2']==board['5']==board['8']=="X" or board['3']==board['6']==board['9']=="X" or board['1']==board['5']==board['9']=="X" or board['3']==board['5']==board['7']=="X" :
        print("PLAYER WON")
    elif board['1']==board['2']==board['3']=="O" or board['4']==board['5']==board['6']=="O" or board['7']==board['8']==board['9']=="O" or board['1']==board['4']==board['7']=="O" or board['2']==board['5']==board['8']=="O" or board['3']==board['6']==board['9']=="O" or board['1']==board['5']==board['9']=="O" or board['3']==board['5']==board['7']=="O" :
        print('COMPUTER WINS')
    else :
        print("IT'S TIE")
result = {}
for round in range(1,11):
    print('ROUND {}'.format(round))
    game()
    result[round] = board
    check_result()
    board = {'1': " ", '2': " ", '3': " ", '4': " ", '5': " ", '6': " ", '7': " ", '8': " ", '9': " "}
print('CHECK RESULT HISTORY')
check = 'yes'
while check=='yes' :
    a = int(input('which round do you want to check:'))
    print(printboard(result[a]))
    check = input('Do you want to check again')


