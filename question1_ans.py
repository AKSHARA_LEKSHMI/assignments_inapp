#create a table
import sqlite3
conn = sqlite3.connect('Cars.db')
cursor=conn.cursor()
cursor.execute('DROP TABLE IF EXISTS CARS')
query="""CREATE TABLE CARS(
        CAR CHAR(20) NOT NULL,
        OWNER CHAR(20) )"""
cursor.execute(query)
conn.commit()
#insert into table
query = ("INSERT INTO CARS (CAR,OWNER)"
             "VALUES (:CAR,:OWNER)")
entries = 0
while entries<=9 :
    params = {'CAR':'','OWNER':''}
    params['CAR'] = input('Enter the car name:')
    params['OWNER'] = input('Enter the owner name:')
    conn.execute(query,params)
    conn.commit()
    entries +=1


cursor = conn.execute("SELECT * from CARS")
print(cursor.fetchall())
conn.close()