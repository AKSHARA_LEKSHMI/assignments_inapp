# create a rock_paper_scissor game
from random import randint
print("Let's play Rock-Paper-Scissors game\nInstructions\nRock beats Scissors\nScissors beats Paper\nPaper beats Rock")
GAME_RESULT = dict()
PLAYER_MOVE = dict()
COMPUTER_MOVE = dict()
moves = ["rock","paper","scissors"]
for round in range (1,11) :
    player_inp = input("Enter your move:")
    computer_move = moves[randint(0,2)]
    if player_inp == computer_move :
        result = ("ROUND {r} tie".format(r=round))
    if player_inp == "rock" :
        if computer_move == "paper" :
            result = ("ROUND {r} computer won".format(r=round))
        else :
            result = ("ROUND {r} player won".format(r=round))
    elif player_inp == "paper" :
        if computer_move == "rock" :
            result = ("ROUND {r} player won".format(r=round))
        else :
            result = ("ROUND {r} computer won".format(r=round))
    elif player_inp == "scissors" :
        if computer_move == "paper" :
            result = ("ROUND {r} player won".format(r=round))
        else :
            result = ("ROUND {r} computer won".format(r=round))
    else :
        print("wrong input")
    GAME_RESULT[round] = result
    PLAYER_MOVE[round] = player_inp
    COMPUTER_MOVE[round] = computer_move
    round = round+1
x = input("Enter the round for which you need the information:")
x = int(x)
print('Player choice = {p}'.format(p=PLAYER_MOVE[x]))
print('Computer choice ={c}'.format(c=COMPUTER_MOVE[x]))
print (GAME_RESULT[x])

