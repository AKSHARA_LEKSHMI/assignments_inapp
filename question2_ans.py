#CREATE TABLE
import sqlite3
conn = sqlite3.connect('SECOND.db')
cursor=conn.cursor()
cursor.execute('DROP TABLE IF EXISTS HOSPITAL')
hospital_sql="""CREATE TABLE HOSPITAL(
        HOSPITAL_ID INT PRIMARY KEY NOT NULL,
        HOSPITAL_NAME CHAR(20) NOT NULL,
        BED_COUNT INT )"""
cursor.execute(hospital_sql)
conn.commit()


cursor.execute('DROP TABLE IF EXISTS DOCTOR')
doctor_sql="""CREATE TABLE DOCTOR(
        DOCTOR_ID INT PRIMARY KEY NOT NULL,
        DOCTOR_NAME CHAR(20) NOT NULL,
        HOSPITAL_ID INT,
        JOINING_DATE CHAR,
        SPECIALITY CHAR(20),
        SALARY INT,
        EXPERIENCE CHAR )"""
cursor.execute(doctor_sql)
conn.commit()

#INSERT INTO HOSPITAL TABLE
conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (1,'Mayo Clinic',200)")

conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (2,'Cleveland Clinic',400)")

conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (3,'Johns Hopkins',1000)")

conn.execute("INSERT INTO HOSPITAL (HOSPITAL_ID,HOSPITAL_NAME,BED_COUNT)"
             "VALUES (4,'UCLA Medical Centre',1500)")
conn.commit()

#INSERT INTO DOCTOR TABLE
conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (101,'DAVID',1,'2005-02-10','PEDIATRIC',40000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (102,'MICHAEL',1,'2018-07-23','ONCOLOGIST',20000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (103,'SUSAN',2,'2016-05-19','GAMACOLOGIST',25000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (104,'ROBERT',2,'2017-12-28','PEDIATRIC',28000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (105,'LINDA',3,'2004-06-04','GAMACOLOGIST',42000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (106,'WILLIAM',3,'2012-09-11','DERMATOLOGIST',30000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (107,'RICHARD',4,'2014-08-21','GAMACOLOGIST',32000,'NULL')")

conn.execute("INSERT INTO DOCTOR (DOCTOR_ID,DOCTOR_NAME,HOSPITAL_ID,JOINING_DATE,SPECIALITY,SALARY,EXPERIENCE)"
             "VALUES (108,'KAREN',4,'2011-10-17','RADIOLOGIST',30000,'NULL')")

conn.commit()
x = input('Enter the speciality of doctor:')
y= int(input('Enter the salary of doctor'))
cursor = conn.execute("SELECT DOCTOR_NAME from DOCTOR where SPECIALITY={} and SALARY>={}".format(x.upper(),y))
print(cursor.fetchall())

def fetch():
    z=int(input('Enter the hospital_id:'))
    final_dict={}
    doc_list=[]
    cursor = conn.execute("SELECT DOCTOR_NAME from DOCTOR where HOSPITAL_ID={}".format(z))
    doc_list.extend(cursor.fetchall())
    l = len(doc_list)
    for i in range(l) :
        cursor = conn.execute("SELECT HOSPITAL_NAME from HOSPITAL where HOSPITAL_ID={}".format(z))
        final_dict[doc_list[i]]=cursor.fetchall()
    print(final_dict)
fetch()
conn.close()
