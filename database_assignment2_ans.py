#CREATE TABLE
import sqlite3
conn = sqlite3.connect('Third.db')
cursor=conn.cursor()
cursor.execute('DROP TABLE IF EXISTS EMPLOYEE')
query="""CREATE TABLE EMPLOYEE(
        EMPLOYEE_ID INT PRIMARY KEY NOT NULL,
        EMPLOYEE_NAME CHAR(20) NOT NULL,
        SALARY INT,
        DEPARTMENT_ID INT )"""
cursor.execute(query)
conn.commit()

#ADD NEW COLUMN
addColumn = "ALTER TABLE EMPLOYEE ADD COLUMN CITY varchar(32)"
cursor.execute(addColumn)

#INSERT DATA INTO EMPLOYEE TABLE
query = ("INSERT INTO EMPLOYEE (EMPLOYEE_ID,EMPLOYEE_NAME,SALARY,DEPARTMENT_ID,CITY)"
             "VALUES (?,?,?,?,?)")
entries = 0
while entries<=4 :
    EMPLOYEE_ID = int(input('Enter the EMPLOYEE_ID:'))
    EMPLOYEE_NAME = input('Enter the EMPLOYEE_NAME:')
    SALARY = int(input('Enter the SALARY:'))
    DEPARTMENT_ID = int(input('Enter the DEPARTMENT_ID:'))
    CITY = input('Enter the CITY:')
    params = (EMPLOYEE_ID,EMPLOYEE_NAME,SALARY,DEPARTMENT_ID,CITY)
    conn.execute(query,params)
    conn.commit()
    entries +=1


cursor = conn.execute("SELECT * from EMPLOYEE")
print(cursor.fetchall())

#read from table
x= int(input('Enter the employee_id:'))
cursor = conn.execute("SELECT EMPLOYEE_NAME,SALARY,DEPARTMENT_ID,CITY from EMPLOYEE where EMPLOYEE_ID={}".format(x))
print(cursor.fetchall())

y=input('Details of employees whose names start with:')
cursor = conn.execute("SELECT EMPLOYEE_ID,EMPLOYEE_NAME,SALARY,DEPARTMENT_ID,CITY from EMPLOYEE where EMPLOYEE_NAME LIKE '{}%'".format(y))
print(cursor.fetchall())

z=int(input('enter the employee_id whose name you want to change:'))
new_name=input('enter the new name of the employee:')
#update=(new_name,z)
#query4 = "UPDATE EMPLOYEE set EMPLOYEE_NAME = %s where EMPLOYEE_ID=%s"
conn.execute("UPDATE EMPLOYEE set EMPLOYEE_NAME = {} where EMPLOYEE_ID={}".format(new_name,z))
conn.commit()
cursor = conn.execute("SELECT * from EMPLOYEE")
print(cursor.fetchall())

#create table departments
cursor.execute('DROP TABLE IF EXISTS DEPARTMENT')
department_sql="""CREATE TABLE DEPARTMENT(
        DEPARTMENT_ID INT PRIMARY KEY NOT NULL,
        DEPARTMENT_NAME CHAR(20) NOT NULL )"""
cursor.execute(department_sql)
conn.commit()
#INSERT DATA INTO DEPARTMENT TABLE
query = ("INSERT INTO DEPARTMENT (DEPARTMENT_ID,DEPARTMENT_NAME)"
             "VALUES (?,?)")
entries = 0
while entries<=4 :
    DEPARTMENT_ID = int(input('Enter the DEPARTMENT_ID:'))
    DEPARTMENT_NAME = input('Enter the DEPARTMENT_NAME:')
    params = (DEPARTMENT_ID,DEPARTMENT_NAME)
    conn.execute(query,params)
    conn.commit()
    entries +=1
cursor = conn.execute("SELECT * from DEPARTMENT")
print(cursor.fetchall())

#PRINT DETAILS OF EMPLOYEE IN A PARTICULAR DEPARTMENT
a= int(input('Enter the department_id:'))
cursor = conn.execute("SELECT EMPLOYEE_ID,EMPLOYEE_NAME,SALARY,CITY from EMPLOYEE where DEPARTMENT_ID={}".format(a))
print(cursor.fetchall())
conn.close()